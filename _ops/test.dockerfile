# Docker dind comme image de base
FROM docker:20.10-dind

# Ajouter Node , npm , curl bash  au cas où python
RUN apk update && apk add --no-cache nodejs npm curl bash python3 py3-pip

# Installer SDK Google Cloud
RUN curl -sSL https://sdk.cloud.google.com | bash

# Ajouter le binaire gcloud au PATH
ENV PATH $PATH:/root/google-cloud-sdk/bin


