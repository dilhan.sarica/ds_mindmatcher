import inquirer from 'inquirer'
import {execa} from 'execa'

async function iValidate(message){
  const name = '__i__'
  const sendChanges = await inquirer.prompt([
    {
      type: 'confirm',
      name,
      message,
      default: true,
    }
  ])
  return sendChanges[name]
}


const root = async () => {
	const { stdout } = await execa('git', ['rev-parse', '--show-toplevel']);
	return stdout;
}


export {iValidate,root}