import {execa} from 'execa'
import chalk from 'chalk'
import { iValidate,root } from './deploy-utils.js'

const workspaceRoot = await root()

// const avec les chemins
const dockerfilePath = `${workspaceRoot}/_ops`
console.log(dockerfilePath, "dockerfilPath")
const dockerimagePath = 'europe-west1-docker.pkg.dev/dashemploi/dashemploi-images/ds-mindmatcher'


let deployEnvironnement = process?.env?.ENVIRONMENT || 'prod'


// Véifier si Docker est en marche
if (!process.env.SKIP_DOCKER){
try {
  await execa('docker', ['stats', '--no-stream'])
} catch (e) {
  console.log(
    chalk.bgRed(
      'Docker is not running. Start it on your side then validate here'
    )
  )
  await iValidate('Docker is now started on the machine')
}
console.log('Start building the requirement container')
}

//
 await execa('docker', [
  'build',
  '--rm',
  '-f',
  `${dockerfilePath}/story.dockerfile`,
  '-t',
  `ds-mindmatcher:${deployEnvironnement}`,
  `${workspaceRoot}`,
],{stdio:'inherit'})


//Tag de l'image 
console.log('Start tagging the image')

await execa('docker', [
  'tag',
  `ds-mindmatcher:${deployEnvironnement}`,
  `${dockerimagePath}:${deployEnvironnement}`,
],{stdio:'inherit'})


// push sur google artifact registry

console.log('Start pushing on Artifact Registry')
await execa('docker', [
  'push',
  `${dockerimagePath}:${deployEnvironnement}`,
],{stdio:'inherit'})

console.log('Succefully Pushed')

// // push sur google cloud run
console.log('Deploying on cloud run ')

await execa(
  'gcloud',
  [
    'run',
    'deploy',
    `ds-mindmatcher-${deployEnvironnement}`,
    `--image=${dockerimagePath}:${deployEnvironnement}`,
    '--region=europe-west1',
    '--memory=1G',
  ],
  { stdio: 'inherit' }
)
