#base image

FROM node:latest 

#Installer PNPM 
RUN npm install -g pnpm

#root folder for application
WORKDIR /app

# Copier les fichiers package.json, package-lock.json et pnpm-lock.yaml
COPY package.json pnpm-lock.yaml ./

# Installer les dépendances
RUN pnpm install

#Copier les fichiers du projet

COPY . .

#construire l'application Storybook
RUN pnpm run build-storybook

#port sur lequel je vais faire tourner mon serveur
EXPOSE 3000

# lancer le serveur :

CMD ["node", "server/start.cjs"]