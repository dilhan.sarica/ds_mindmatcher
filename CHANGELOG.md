# @mmorg/ds-mindmatcher

## 0.1.1

### Patch Changes

- upgrade to mantine v7

## 0.1.0

### Minor Changes

- e5dad15: Init Matchin proman UI components
