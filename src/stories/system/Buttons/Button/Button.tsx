import { Button as MantineButton, useProps } from "@mantine/core"
import React from "react"

const defaultProps = {}

export const Button = (props: any) => {
  const { ...attr } = useProps("Button", defaultProps, props)

  return <MantineButton {...attr}>{props?.children}</MantineButton>
}
