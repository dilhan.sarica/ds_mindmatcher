import { ArrowRight, DeviceFloppy } from "tabler-icons-react"

import { Button } from "./Button"
import React from "react"
import { linkTo } from "@storybook/addon-links"

export default {
  component: Button,
  parameters: {},
  decorators: [
    (Story: any) => (
      <div style={{ width: "300px" }}>
        <Story />
      </div>
    )
  ]
}

export const Default = {
  render: (args: any) => (
    <Button {...args} rightIcon={<ArrowRight size={16} />} />
  ),
  args: {
    children: "Valider mon profil"
  }
}

export const SaveForLater = {
  render: (args: any) => (
    <Button {...args} leftIcon={<DeviceFloppy size={22} strokeWidth={1} />} />
  ),
  args: {
    children: "Reprendre plus tard",
    variant: "outline"
  }
}

export const goToOtherStory = {
  render: (args: any) => (
    <Button {...args} leftIcon={<DeviceFloppy size={22} strokeWidth={1} />} />
  ),
  args: {
    children: "Go to other story",
    onClick: () => {
      linkTo("profile-missions", "Default")()
    }
  }
}
