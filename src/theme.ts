export const globalStyles = {
  "*, *::before, *::after": {
    boxSizing: "border-box"
  },

  ".your-class": {
    backgroundColor: "red"
  },

  "#your-id > [data-active]": {
    backgroundColor: "pink"
  },

  ".line-clamp": {
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap"
  },

  ".line-clamp-2": {
    overflow: "hidden",
    display: "-webkit-box",
    WebkitLineClamp: "2",
    WebkitBoxOrient: "vertical"
  },

  ".line-clamp-3": {
    overflow: "hidden",
    display: "-webkit-box",
    WebkitLineClamp: "3",
    WebkitBoxOrient: "vertical"
  }
}

export const components = {}

export const theme = {
  globalStyles: (theme: any) => ({
    ...globalStyles,
    body: {
      ...theme.fn.fontStyles(),
      backgroundColor:
        theme.colorScheme === "dark" ? theme.colors.dark[7] : theme.white,
      color: theme.colorScheme === "dark" ? theme.colors.dark[0] : theme.black,
      lineHeight: theme.lineHeight
    }
  }),

  components: components,

  radius: {
    md: "12px"
  },

  // ** FONTS
  fontFamily: "Inter, sans-serif",
  headings: { fontFamily: "Inter, sans-serif" },

  // **********
  // * COLORS
  // White and black colors, defaults to '#fff' and '#000'
  white: "#fff",
  black: "#111217",
  defaultRadius: 0
}

// Theme Object
// * https://mantine.dev/theming/theme-object/
