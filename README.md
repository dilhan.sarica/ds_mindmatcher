# MindMatcher Design System

## About MindMatcher Design System

The MindMatcher Design System is a cohesive collection of design principles, rules, and UI/UX components used to create a consistent and efficient user experience across all MindMatcher applications. This system aims to improve the consistency, efficiency, and maintainability of our digital products.

## Quick Start

To quickly install and start using the MindMatcher Design System, run:

```bash
pnpm install 
```
To run the app in dev :

```bash
pnpm run storybook
```

## Usage

To use the MindMatcher Design System components in your projects, import and use them like this:

```jsx
"In progress"
```

## Documentation


For more detailed information on how to use the MindMatcher Design System, refer to our [documentation](https://www.notion.so/Comprendre-l-inter-t-du-Design-System-a6ca46ea59804aa7b3e725bb118f84d2). This includes comprehensive guides, API references, and examples.

For more detailed information on storybook our front stack, refer to our [articles](https://www.notion.so/Dev-AI-Coders-c8ab9d9e2fb74e5d961f2033d23905cb). 


## BUILT WITH

  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" height="40" alt="typescript logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original.svg" height="40" alt="react logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/storybook/storybook-original.svg" height="40" alt="storybook logo"  />
  <img width="12" />
 