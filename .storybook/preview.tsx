import {
  MantineProvider,
  DEFAULT_THEME,
  MantineThemeOther,
  MantineThemeColors
} from "@mantine/core"
import "@mantine/core/styles.css"
import { ConfigProvider } from "antd"
import React from "react"
import { theme as jobsongTheme } from "../src/theme"

export const withTheme = (Story, context) => {
  return (
    <ConfigProvider
      theme={{
        token: {
          colorPrimary: "#F28482",
          fontFamily: "Rubik",
          fontSize: 16
        }
      }}
    >
      <MantineProvider theme={jobsongTheme as any}>
        <Story />
      </MantineProvider>
    </ConfigProvider>
  )
}

/** @type { import('@storybook/react').Preview } */
const preview = {
  decorators: [withTheme],
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/
      }
    },
    viewport: {
      defaultViewport: "responsive"
    },
    layout: "fullscreen"
  }
}

export const globalTypes = {
  theme: {
    name: "Theme",
    description: "Global theme for components",
    defaultValue: "light",
    toolbar: {
      // The icon for the toolbar item
      icon: "circlehollow",
      // Array of options
      items: [
        { value: "jobsong", icon: "circlehollow", title: "jobsong" },
        { value: "proman", icon: "circle", title: "proman" }
      ],
      // Property that specifies if the name of the item will be displayed
      showName: true
    }
  }
}

export default preview
