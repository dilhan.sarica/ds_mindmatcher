/** @type { import('@storybook/react-vite').StorybookConfig } */

import { mergeConfig } from 'vite';
import path from 'path';

const config = {
  stories: ['../src/stories/**/*.mdx', '../src/stories/**/*.stories.@(js|jsx|mjs|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-onboarding',
    '@storybook/addon-interactions'
  ],
  framework: {
    name: '@storybook/react-vite',
    options: {}
  },
  docs: {
    autodocs: 'tag'
  },
  async viteFinal(config, options) {
    // Add your configuration here
    return mergeConfig(
      {
        ...config,
        resolve: {
          alias: [
            {
              find: 'stories',
              replacement: path.resolve(__dirname, '../stories')
            }
          ]
        }
      },
      {
        define: { 'process.env': {} }
      }
    );
  }
};
export default config;