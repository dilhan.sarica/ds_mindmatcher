/* eslint-disable no-undef */
const fastify = require("fastify")({ logger: true })
const path = require("path")
const fastifyStatic= require("@fastify/static")

const storybookStatic = path.join(__dirname,"..","storybook-static")
const port = process.env.PORT || "3000"
const host = "0.0.0.0"
fastify.register(fastifyStatic, {
  root: storybookStatic,
  prefix:"/"
})

fastify.get("/", async (request, reply) => {
  return reply.sendFile("index.html") 
})

const start = async () => {
  try {
    await fastify.listen({port,host})
    console.log(`Server is running on http://localhost:${port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()
